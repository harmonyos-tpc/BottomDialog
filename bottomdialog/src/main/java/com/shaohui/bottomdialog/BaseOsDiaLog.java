package com.shaohui.bottomdialog;
/*----------------------------------------------------------------------------
 * Copyright (c) <2013-2018>, <Huawei Technologies Co., Ltd>
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 * to endorse or promote products derived from this software without specific prior written
 * permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 * Notice of Export Control Law
 * ===============================================
 * Huawei LiteOS may be subject to applicable export control laws and regulations, which might
 * include those applicable to Huawei LiteOS of U.S. and the country in which you are located.
 * Import, export and usage of Huawei LiteOS in any manner by you shall be in compliance with such
 * applicable export control laws and regulations.
 *---------------------------------------------------------------------------*/
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

public abstract class BaseOsDiaLog extends CommonDialog {
    private Context mContext;

    public BaseOsDiaLog(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        Component component = LayoutScatter.getInstance(mContext)
                .parse(getDiaLogLayout(), null, true);
        setContentCustomComponent(component);
        onCreateDiaLog(component);
        setAlignment(getAlignment());
        setSize(getDiaLogSize().width, getDiaLogSize().height);
    }

    /**
     * dialog布局
     *
     * @return str
     */
    protected abstract int getDiaLogLayout();

    /**
     * 逻辑处理
     *
     * @param component str
     */
    protected void onCreateDiaLog(Component component) {}

    /**
     * 设置宽高
     * 默认 800*500
     *
     * @return str
     */
    protected DiaLogSize getDiaLogSize() {
        return new DiaLogSize(800, 500);
    }

    /**
     * 对齐方式
     * 默认居中
     *
     * @return str
     */
    protected int getAlignment() {
        return TextAlignment.CENTER;
    }


    @Override
    protected void onHide() {
        super.onHide();
        destroy();
    }

    public class DiaLogSize {
        private int width;
        private int height;

        public DiaLogSize(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }
}
