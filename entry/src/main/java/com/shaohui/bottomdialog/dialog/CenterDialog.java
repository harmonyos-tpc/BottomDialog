package com.shaohui.bottomdialog.dialog;

import com.shaohui.bottomdialog.ResourceTable;
import com.shaohui.bottomdialog.utils.DisplayUtils;
import com.shaohui.bottomdialog.BaseOsDiaLog;
import ohos.agp.components.Component;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

public class CenterDialog extends BaseOsDiaLog {
    private Context mContext;
    public CenterDialog(Context context) {
        super(context);
        this.mContext=context;
    }

    @Override
    protected int getDiaLogLayout() {
        return ResourceTable.Layout_dialog_bottom_share;
    }

    @Override
    protected int getAlignment() {
        return TextAlignment.CENTER;
    }

    @Override
    protected DiaLogSize getDiaLogSize() {
        return new DiaLogSize(DisplayUtils.getWindowWidth(mContext), 300);
    }

    @Override
    protected void onCreateDiaLog(Component component) {
        super.onCreateDiaLog(component);
        //逻辑处理
//       component.findComponentById();
    }

    public static void showCenterDialog(Context context) {
        new CenterDialog(context)
                .setAutoClosable(true)
                .show();
    }
}
