package com.shaohui.bottomdialog.slice;

import com.shaohui.bottomdialog.ResourceTable;
import com.shaohui.bottomdialog.base.BaseAbilitySlice;
import com.shaohui.bottomdialog.dialog.BottomDialog;
import com.shaohui.bottomdialog.dialog.CenterDialog;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.multimodalinput.event.KeyBoardEvent;
import ohos.multimodalinput.event.KeyEvent;

public class MainAbilitySlice extends BaseAbilitySlice {
    @Override
    protected int getLayout() {
        return ResourceTable.Layout_ability_main;
    }

    @Override
    protected void initView() {
        Button bottomDialog = (Button) findComponentById(ResourceTable.Id_but_bottom_dialog);
        Button editDialog = (Button) findComponentById(ResourceTable.Id_but_edit_dialog);


        bottomDialog.setClickedListener(component -> {
            //
            BottomDialog.showBottomDialog(this);
        });
        editDialog.setClickedListener(component -> {
            //
//            EditTextDialog.showEditDialog(this);

            CenterDialog.showCenterDialog(this);
        });

    }

    @Override
    protected void initData() {

    }


    private void keyEvent() {

        KeyBoardEvent keyBoardEvent = new KeyBoardEvent() {
            @Override
            public int getSourceDevice() {
                return 0;
            }

            @Override
            public String getDeviceId() {
                return null;
            }

            @Override
            public int getInputDeviceId() {
                return 0;
            }

            @Override
            public long getOccurredTime() {
                return 0;
            }

            @Override
            public boolean isKeyDown() {
                return false;
            }

            @Override
            public int getKeyCode() {
                return 0;
            }

            @Override
            public long getKeyDownDuration() {
                return 0;
            }

            @Override
            public boolean isNoncharacterKeyPressed(int i) {
                return false;
            }

            @Override
            public boolean isNoncharacterKeyPressed(int i, int i1) {
                return false;
            }

            @Override
            public boolean isNoncharacterKeyPressed(int i, int i1, int i2) {
                return false;
            }

            @Override
            public int getUnicode() {
                return 0;
            }
        };
        Component.KeyEventListener keyEventListener = new Component.KeyEventListener() {
            @Override
            public boolean onKeyEvent(Component component, KeyEvent keyEvent) {
                return false;


            }
        };
    }

}
