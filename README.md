# BottomDialog_openharmony  

## 预览
   ![image](image/demo1.gif)

## 集成配置 


```
方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```


```
方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}

implementation 'io.openharmony.tpc.thirdlib:bottomDialog-library:1.0.0'
```


### 1.使用封装baseDialog

public class BottomDialog extends BaseOsDiaLog {
    private Context mContext;

    public BottomDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected int getDiaLogLayout() {
        return ResourceTable.Layout_dialog_bottom_share;
    }

    @Override
    protected int getAlignment() {
        return TextAlignment.BOTTOM;
    }

    @Override
    protected DiaLogSize getDiaLogSize() {
        return new DiaLogSize(DisplayUtils.getWindowWidth(mContext), 300);
    }

    @Override
    protected void onCreateDiaLog(Component component) {
        super.onCreateDiaLog(component);
        //逻辑处理

    }

    public static void showBottomDialog(Context context) {
        new BottomDialog(context)
                .setAutoClosable(true)
                .show();
    }
}

### 2. main内使用


BottomDialog.showBottomDialog(this);


## License

    Copyright 2016 shaohui10086

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.